'use strict';function _asyncToGenerator(fn) {return function () {var gen = fn.apply(this, arguments);return new Promise(function (resolve, reject) {function step(key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {return Promise.resolve(value).then(function (value) {return step("next", value);}, function (err) {return step("throw", err);});}}return step("next");});};}const exec = require('promised-exec');
const path = require('path');
const fs = require('fs');

const sensorProgramPath = '/srv/rasp2-temp-humidity/app/startup';

let sensorData = {};
let sensorDatas = [];
let sensorDataHistory = [];

const collectSensorData = (() => {var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {var 
        result;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) switch (_context.prev = _context.next) {case 0:_context.prev = 0;_context.next = 3;return (

                        exec(sensorProgramPath));case 3:result = _context.sent;
                    result = JSON.parse(result);
                    sensorData = result;
                    sensorData.time = new Date();
                    sensorDatas.push(sensorData);
                    console.log(`${ sensorData.time } Temp: ${ sensorData.temperature }, Humidity ${ sensorData.humidity }`);_context.next = 14;break;case 11:_context.prev = 11;_context.t0 = _context['catch'](0);

                    console.error('collectSensorData error: ', _context.t0);case 14:case 'end':return _context.stop();}}, _callee, undefined, [[0, 11]]);}));return function collectSensorData() {return ref.apply(this, arguments);};})();



const flushSensorData = () => {
    const time = new Date();
    let fileName = `data/sensor-${ time.getFullYear() }-${ time.getMonth() + 1 }-${ time.getDate() }-${ time.getHours() }-${ time.getMinutes() }.json`;
    let fileContent = JSON.stringify(sensorDatas, null, 4);
    sensorDatas = [];
    fs.writeFile(fileName, fileContent, err => {
        if (err) console.error(`flushSensorData:fs.writeFile:callback`, err);else 
        console.log(`${ new Date() } flushSensorData`);});};



// Interval for collecting data, 2s
setInterval(collectSensorData, 2000);
// Interval for flush historical data, 10s
setInterval(flushSensorData, 60000);

function getSensorData() {
    return sensorData;}


function getSensorDataHistory() {
    return sensorDataHistory;}


module.exports = { 
    getSensorData, 
    getSensorDataHistory };