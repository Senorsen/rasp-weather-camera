'use strict';const express = require('express');
const router = express.Router();
const sensor = require('../service/sensor');

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Group 15 温湿度监控图表' });});


router.get('/api/getSensorData', (req, res) => {
  res.json(sensor.getSensorData());});


module.exports = router;