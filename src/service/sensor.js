const exec = require('promised-exec');
const path = require('path');
const fs = require('fs');

const sensorProgramPath = '/srv/rasp2-temp-humidity/app/startup';

let sensorData = {};
let sensorDatas = [];
let sensorDataHistory = [];

const collectSensorData = async () => {
    let result;
    try {
        result = await exec(sensorProgramPath);
        result = JSON.parse(result);
        sensorData = result;
        sensorData.time = new Date();
        sensorDatas.push(sensorData);
        console.log(`${sensorData.time} Temp: ${sensorData.temperature}, Humidity ${sensorData.humidity}`);
    } catch (e) {
        console.error('collectSensorData error: ', e);
    }
};

const flushSensorData = () => {
    const time = new Date();
    let fileName = `data/sensor-${time.getFullYear()}-${time.getMonth() + 1}-${time.getDate()}-${time.getHours()}-${time.getMinutes()}.json`;
    let fileContent = JSON.stringify(sensorDatas, null , 4);
    sensorDatas = [];
    fs.writeFile(fileName, fileContent, (err) => {
        if (err) console.error(`flushSensorData:fs.writeFile:callback`, err);
        else console.log(`${new Date()} flushSensorData`);
    });
};

// Interval for collecting data, 2s
setInterval(collectSensorData, 2000);
// Interval for flush historical data, 10s
setInterval(flushSensorData, 60000);

function getSensorData() {
    return sensorData;
}

function getSensorDataHistory() {
    return sensorDataHistory;
}

module.exports = {
    getSensorData,
    getSensorDataHistory
};

